**25/02/2019**

**v0.1.3**

  - refactor repository to support multiple builds
  - add build variant ubuntu-buildimage-sdl


**01/05/2018**

**v0.1.1**

  - change tagging scheme  
  - support multiple image builds  
  - add docker stack


**24/01/2018**

**16.04**

    - first version
