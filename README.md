# ubuntu-buildimage

    ubuntu baseimage with build support

## Tags

    mswinson/ubuntu-buildimage:xenial
    mswinson/ubuntu-buildimage:artful
    mswinson/ubuntu-buildimage:bionic
    mswinson/ubuntu-buildimage:xenial-sdl
    mswinson/ubuntu-buildimage:artful-sdl
    mswinson/ubuntu-buildimage:bionic-sdl


## Usage

run with default user (ubuntu)

    docker run -it --rm mswinson/ubuntu-buildimage:<version>


run with local user id

    docker run -it --rm -e LOCAL_USER_ID=`id -u $USER` mswinson/ubuntu-buildimage:<version>


run with user name

    docker run -it --rm -e LOCAL_USER_ID=`id -u $USER` -e USER_NAME=developer mswinson/ubuntu-buildimage:<version>


## Image variants

### mswinson/ubuntu-buildimage:<release>

from  

    mswinson/ubuntu-baseimage:<release>

packages  

    build-essential

    llvm
    clang
    clang-tidy
    makedepends


### mswinson/ubuntu-buildimage:<release>-sdl

from  
  
    mswinson/ubuntu-buildimage:<release>

packages  

    libsdl2
    libsdl2-dev


