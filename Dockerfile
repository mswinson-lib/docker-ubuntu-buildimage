FROM hashicorp/packer

RUN apk add --update make
RUN apk add --update curl
RUN apk add --update jq
RUN apk add --update docker
